package drawing.ui;

import drawing.handlers.*;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

import java.lang.reflect.InvocationTargetException;

public class ToolBar extends HBox {

    private Button clearButton;
    private Button rectangleButton;
    private Button circleButton;
    private Button triangleButton;
    private Button removeShapes;

    public ToolBar(DrawingPane drawingPane) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        ButtonFactory buttonFactory = new ButtonFactory(drawingPane);
        clearButton = buttonFactory.createButton(ButtonFactory.BUTTON_CLEAR, ButtonFactory.TEXT_ONLY);
        clearButton.addEventFilter(ActionEvent.ACTION, new ClearButtonHandler(drawingPane));

        rectangleButton = buttonFactory.createButton(ButtonFactory.BUTTON_RECTANGLE, ButtonFactory.TEXT_ONLY);
        rectangleButton.addEventFilter(ActionEvent.ACTION, new RectangleButtonHandler(drawingPane));

        circleButton = buttonFactory.createButton(ButtonFactory.BUTTON_CIRCLE, ButtonFactory.TEXT_ONLY);
        circleButton.addEventFilter(ActionEvent.ACTION, new EllipseButtonHandler(drawingPane));

        triangleButton = buttonFactory.createButton(ButtonFactory.BUTTON_TRIANGLE, ButtonFactory.TEXT_ONLY);
        triangleButton.addEventFilter(ActionEvent.ACTION, new TriangleButtonHandler(drawingPane));

        removeShapes = buttonFactory.createButton(ButtonFactory.BUTTON_REMOVE, ButtonFactory.TEXT_ONLY);
        removeShapes.addEventFilter(ActionEvent.ACTION, new DeleteButtonHandler(drawingPane));

        this.getChildren().addAll(clearButton, rectangleButton, circleButton, triangleButton, removeShapes);

        this.setPadding(new Insets(5));
        this.setSpacing(5.0);
        this.getStyleClass().add("toolbar");

    }
}