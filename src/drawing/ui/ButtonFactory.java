package drawing.ui;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.lang.reflect.InvocationTargetException;

public class ButtonFactory {

    public static final String BUTTON_CLEAR = "Clear";
    public static final String BUTTON_CIRCLE = "Circle";
    public static final String BUTTON_RECTANGLE = "Rectangle";
    public static final String BUTTON_TRIANGLE = "Triangle";
    public static final String BUTTON_REMOVE = "Delete";

    public static final boolean TEXT_ONLY = true;
    public static final boolean ICONS_ONLY = false;

    private DrawingPane drawingPane;

    ButtonFactory(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public Button createButton(String buttonName, boolean textOnly) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Button button = new Button();
        if (textOnly) {
            button.setText(buttonName);
        } else {
            Image img = new Image(getClass().getResourceAsStream("../images/" + buttonName.toLowerCase() + ".png"));
            ImageView view = new ImageView(img);
            view.setFitHeight(20);
            view.setPreserveRatio(true);
            button.setGraphic(view);
            button.setTooltip(new Tooltip(buttonName));
        }

        return button;
    }
}