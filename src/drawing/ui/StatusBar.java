package drawing.ui;

import drawing.Observer;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class StatusBar extends HBox implements Observer {

    private Label label;
    private Label selectedShapeslabel;
    private DrawingPane dpane;

    public StatusBar(DrawingPane dpane) {
        this.dpane = dpane;
        this.label = new Label("0 forme(s)");
        this.selectedShapeslabel = new Label("( 0 selectionnée(s) )");
        this.getChildren().add(this.label);
        this.getChildren().add(this.selectedShapeslabel);
    }

    public int getNbShapes() {
        return dpane.getNbShapes();
    }

    public int getNbSelectedShapes() {
        return dpane.getNbSelectedShapes();
    }

    @Override
    public void update() {
        this.label.setText(dpane.getNbShapes() + " forme(s)");
        this.selectedShapeslabel.setText("( " + dpane.getNbSelectedShapes() + " selectionnée(s) )");
    }
}
