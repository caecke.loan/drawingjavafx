import drawing.shapes.IShape;
import drawing.PaintApplication;
import drawing.shapes.ShapeAdapter;
import drawing.ui.StatusBar;
import javafx.scene.input.KeyCode;
import javafx.scene.shape.Ellipse;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.util.Iterator;

import static org.junit.Assert.*;

public class PaintTest extends ApplicationTest {

    PaintApplication app;

    @Override
    public void start(Stage stage) {
        try {
            app = new PaintApplication();
            app.start(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_draw_circle_programmatically() {
        interact(() -> {
            app.getDrawingPane().addShape(new ShapeAdapter(new Ellipse(20, 20, 30, 30)));
        });
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_circle() {
        // given:
        clickOn("Circle");
        moveBy(60, 60);

        // when:
        drag().dropBy(30, 30);
        //press(MouseButton.PRIMARY); moveBy(30,30); release(MouseButton.PRIMARY);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_rectangle() {
        // given:
        clickOn("Rectangle");
        moveBy(0, 60);

        // when:
        drag().dropBy(70, 40);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_draw_triangle() {
        // given:
        clickOn("Triangle");
        moveBy(10, 50);

        // when:
        drag().dropBy(70, 40);

        // then:
        Iterator it = app.getDrawingPane().iterator();
        assertTrue(it.next() instanceof IShape);
        assertFalse(it.hasNext());
    }

    @Test
    public void should_clear() {
        // given:
        clickOn("Rectangle");
        moveBy(30, 60).drag().dropBy(70, 40);
        clickOn("Circle");
        moveBy(-30, 160).drag().dropBy(70, 40);

        // when:
        clickOn("Clear");

        // then:
        assertFalse(app.getDrawingPane().iterator().hasNext());
    }

    @Test
    public void should_update_status_bar_on_add() {
        // given:
        clickOn("Triangle");
        moveBy(10, 50);
        // when:
        drag().dropBy(70, 40);
        // then:
        StatusBar s = app.getStatusBar();
        assertEquals(1, s.getNbShapes());
    }

    @Test
    public void should_update_status_bar_on_clear() {
        // given:
        clickOn("Rectangle");
        moveBy(30, 60).drag().dropBy(70, 40);
        clickOn("Circle");
        moveBy(-30, 160).drag().dropBy(70, 40);
        clickOn("Triangle");
        moveBy(50, 100).drag().dropBy(70, 230);
        // when:
        clickOn("Clear");
        // then:
        StatusBar s = app.getStatusBar();
        assertEquals(0, s.getNbShapes());
    }

    @Test
    public void should_update_status_bar_on_select() {
        // given:
        clickOn("Rectangle");
        moveBy(30, 60).drag().dropBy(70, 40);
        clickOn("Circle");
        moveBy(-30, 160).drag().dropBy(70, 40);
        clickOn("Triangle");
        moveBy(50, 100).drag().dropBy(70, 230);
        // when
        press(KeyCode.SHIFT).clickOn();

        // then:
        StatusBar s = app.getStatusBar();
        assertEquals(1, s.getNbSelectedShapes());
    }

    @Test
    public void deleteForm() {
        StatusBar statusBar = app.getStatusBar();
        clickOn("Rectangle");
        moveBy(30, 60).drag().dropBy(70, 40);
        clickOn();
        assertEquals(1, statusBar.getNbShapes());
        assertEquals(1, statusBar.getNbSelectedShapes());

        clickOn("Delete");

        assertEquals(0, statusBar.getNbShapes());
        assertEquals(0, statusBar.getNbSelectedShapes());

    }

    @Test
    public void deleteFormMultiple() {
        StatusBar statusBar = app.getStatusBar();
        clickOn("Rectangle");
        moveBy(30, 60).drag().dropBy(70, 40);

        clickOn("Circle");
        moveBy(-30, 160).drag().dropBy(70, 40);

        press(KeyCode.SHIFT).clickOn();
        press(KeyCode.SHIFT).moveBy(-50, -105).clickOn().release(KeyCode.SHIFT);

        assertEquals(2, statusBar.getNbShapes());
        assertEquals(2, statusBar.getNbSelectedShapes());

        clickOn("Delete");

        assertEquals(0, statusBar.getNbShapes());
        assertEquals(0, statusBar.getNbSelectedShapes());
    }

}